# Git

- Directory structure
- Git merge
- Git rebase

---
# Git directory

![Git directory](https://gitlab.com/datnt/git-demo/raw/master/images/git-directory.png)

---
## Git logs

![Git directory detail](https://gitlab.com/datnt/git-demo/raw/master/images/git-directory-logs.png)

---
## Git refs

![Git directory detail](https://gitlab.com/datnt/git-demo/raw/master/images/git-directory-refs.png)

---
## Git staging

![Git directory detail](https://gitlab.com/datnt/git-demo/raw/master/images/git-directory-index.png)

---

## Git object

![Git directory detail](https://gitlab.com/datnt/git-demo/raw/master/images/git-directory-objects.png)

---

![Git object](https://gitlab.com/datnt/git-demo/raw/master/images/git-object.png)

```
LAZ-VN-L-M-1288:.git dat.nguyen1$ git cat-file -p 27899d9
100644 blob 797b705f6e8ff19cb087263cb82fdffc7b74433c    .gitignore
100644 blob de89bdb17d43e9110a2635b7c1d8cbf5f5284174    PITCHME.md
040000 tree 5e32539ce6170050d1d2c8e4759070f6e083a866    images
```

---
# Git flow

![Add-commit-checkout](https://gitlab.com/datnt/git-demo/raw/master/images/git-flow.png)

---
# Git merge

![Merge](https://developer.atlassian.com/blog/2014/12/pull-request-merge-strategies-the-great-debate/what-is-a-merge.gif)

---
# Git rebase
![Rebase](https://developer.atlassian.com/blog/2014/12/pull-request-merge-strategies-the-great-debate/what-is-a-rebase.gif)

---
# Merge or rebase?

---
# Reference

- [Understanding git for real by exploring the .git directory](https://medium.freecodecamp.org/understanding-git-for-real-by-exploring-the-git-directory-1e079c15b807)
- [Understanding Git — Index](https://hackernoon.com/understanding-git-index-4821a0765cf)
- [Git: Understanding the Index File](https://mincong-h.github.io/2018/04/28/git-index/)
